package com.example.mybookshopapp.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class AuthorService {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public AuthorService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Map<Character, List<Author>> getIndexedAuthors() {
        return getAuthors().stream().collect(Collectors.groupingBy(author -> author.getLastName().charAt(0)));
    }

    public List<Author> getAuthors() {
        return jdbcTemplate.query(
                "SELECT * FROM authors ORDER BY lastName, firstName ASC",
                new BeanPropertyRowMapper<>(Author.class)
        );
    }

}
