package com.example.mybookshopapp.controllers;

import com.example.mybookshopapp.data.Author;
import com.example.mybookshopapp.data.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Controller
public class AuthorsController {

    private final AuthorService authorService;

    @Autowired
    public AuthorsController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @GetMapping("/bookshop/authors")
    public String authors(Model model) {
        Map<Character, List<Author>> indexedAuthors = authorService.getIndexedAuthors();
        Set<Character> index = indexedAuthors.keySet();

        model.addAttribute("index", index);
        model.addAttribute("indexedAuthors", indexedAuthors);

        return "authors/index";
    }

}
